import React, { useContext, useEffect, useState } from 'react'

import quagga from 'quagga'

import { BasketContext } from '../context/Basket'

import './Scanner.css'

const Scanner = () => {

  const { basket, dispatchBasket } = useContext(BasketContext)
  const [ scanReady, setScanReady ] = useState(true);

  useEffect(() => {
    quagga.init({
      inputStream: {
        name: "Live",
        type: "LiveStream",
        target: document.querySelector('#scanner-window')
      },
      decoder: {
        readers: ["code_128_reader", "ean_reader", "ean_8_reader"]
      },
      frequency: 1
    }, function (err) {
      if (err) return console.error(err);
      quagga.onDetected(async data => {
        quagga.pause()
        dispatchBasket({ type: 'ADD_ITEM', payload: { ean: data?.codeResult?.code, quantity: 1 }})
        setTimeout(() => quagga.start(), 1000)
      });
      quagga.start();
      // setScanReady(true);
    });
  },[ ])

  return (
    <div id="scanner-window">
      <button onClick={() => dispatchBasket({ type: 'ADD_ITEM', payload: { ean: 1234, quantity: 3 } })}>test</button>
    </div>
  )
}

export default Scanner