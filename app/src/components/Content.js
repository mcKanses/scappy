import React, { useContext, useEffect } from 'react'

import ItemsList from './ItemsTable'
import Scanner from './Scanner'

import { AppContext } from '../context/App'

import './Content.css'

const Content = () => {

  const { appState, dispatchAppState } = useContext(AppContext)

  useEffect(() => {
    console.log(appState)
  },[appState])

  return (
    <div id="content">
      {appState?.state === 'scan'
        ? <>
            <Scanner>Nur ein Test</Scanner>
            <ItemsList />
          </>
        : <>
            <h1>Klicken Sie auf den Button,<br/>um mit dem Scannen anzufangen</h1>
            <button onClick={() => dispatchAppState({ type: 'START_SCANNING'  })}>Scannen</button>
          </>
      }
    </div>
  )
}

export default Content