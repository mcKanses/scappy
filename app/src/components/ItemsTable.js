import React, { useContext } from 'react'

import { BasketContext } from '../context/Basket'

import './ItemsTable.css'

const ItemsTable = () => {

  const { basket, dispatchBasket } = useContext(BasketContext)

  return (
    <table id="items-table" >
    <thead>
      <tr>
        <th>Anzahl</th>
        <th>Bezeichnung</th>
        <th className="price">Preis<br/>einzeln</th>
        <th className="price total">Preis<br/>gesamt</th>
      </tr>
    </thead>
    <tbody>
      {basket?.items.map(({ ean, price, quantity }) => (

        <tr key={ean}>
          <td className="quantity">{quantity}</td>
          <td className="eanOrName">{ean}</td>
          <td className="price">€&nbsp;{price},-</td>
          <td className="price total">€&nbsp;{quantity * price}</td>
        </tr>
      ))}
    </tbody>
    </table>
  )
}

export default ItemsTable