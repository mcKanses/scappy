import React from 'react'

import './Header.css'

const Header = () => {
  return (
    <div id="header"><h1>scappy</h1></div>
  )
}

export default Header