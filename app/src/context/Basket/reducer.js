import produce from 'immer'

const BasketReducer = (state, { type, payload }) => {
  switch (type) {
    case 'ADD_ITEM':
      return produce( state, draftState => {
        const itemExists = draftState.items.find(({ ean }) => ean === payload?.ean);
        if (!itemExists) {
          draftState.items.push({
            ...payload,
            price: Math.ceil(Math.random() * 100)
          });
          return;
        }
        itemExists.quantity += payload?.quantity || 1;
      } );
    default:
      return state;
  }
}

export default BasketReducer;