import React, { createContext, useReducer } from 'react'
import BasketReducer from './reducer'
import initialState from './initialState'

export const BasketContext = createContext(initialState)

const BasketProvider = ({ children }) => {
  const [basket, dispatchBasket] = useReducer(BasketReducer, initialState)
  return (
    <BasketContext.Provider value={{ basket, dispatchBasket }}>
      {children}
    </BasketContext.Provider>
  )
}

export default BasketProvider