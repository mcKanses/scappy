const AppReducer = (state, action) => {
  switch(action.type) {
    case "START_SCANNING":
      return {
        ...state,
        state: 'scan'
      }
    default:
      return state
  }
}

export default AppReducer