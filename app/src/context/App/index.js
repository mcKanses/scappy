import React, { createContext, useReducer } from "react";
import AppReducer from "./reducer";
import initialState from "./initialState";

export const AppContext = createContext(initialState);

const AppStateProvider = ({ children }) => {
  const [appState, dispatchAppState] = useReducer(AppReducer, initialState);
  return (
    <AppContext.Provider value={{ appState, dispatchAppState }}>
      {children}
    </AppContext.Provider>
  )
}

export default AppStateProvider;
