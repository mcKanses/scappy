import logo from './logo.svg';

// tmp
import quagga from 'quagga'

// components
import Header from './components/Header';
import Content from './components/Content';

function App() {
  return (
    <>
      <Header />
      <Content />
    </>
  );
}

export default App;
